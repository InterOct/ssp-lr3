﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;

namespace LR3
{
    [WebService]
    [WebServiceBinding]
    [ToolboxItem(false)]
    [ScriptService]
    public partial class WebService : System.Web.Services.WebService
    {

        [WebMethod]
        public string Encrypt(string s, int key)
        {
            var newS = "";
            foreach (char ch in s)
            {
                newS += (char) (ch + key);
            }
            return newS;
        }

        [WebMethod]
        public string Decrypt(string s, int key)
        {
            var newS = "";
            foreach (char ch in s)
            {
                newS += (char)(ch - key);
            }
            return newS;
        }

    }
}